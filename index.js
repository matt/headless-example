import puppeteer from "puppeteer-core";

const browser = await puppeteer.launch();
const page = await browser.newPage();
await page.goto("https://www.google.com", { waitUntil: "networkidle0" });
await page.screenshot({ path: "google.png" });

await browser.close();
